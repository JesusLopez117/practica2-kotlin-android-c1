package com.example.practica2c2kotlin

//Librerias necesarias
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var lblTotal: TextView
    private lateinit var txtPeso: EditText
    private lateinit var txtAltura: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        lblTotal = findViewById(R.id.lblTotal)
        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)

        //Codificación del boton calcular
        btnCalcular.setOnClickListener {
            if (txtPeso.text.toString().isEmpty() && txtAltura.text.toString().isEmpty()) {
                Toast.makeText(this@MainActivity, "FAVOR DE INGRESAR LOS DATOS", Toast.LENGTH_SHORT).show()
            } else {
                var peso = 0.0F
                var altura = 0.0F
                var total = 0.0F
                peso = txtPeso.text.toString().toFloat()
                altura = txtAltura.text.toString().toFloat()
                total = peso / (altura * altura)
                lblTotal.text = total.toString()
            }
        }

        //Codificación del boton limpiar
        btnLimpiar.setOnClickListener {
            lblTotal.text = ""
            val vacio: Editable = Editable.Factory.getInstance().newEditable("")
            txtAltura.text = vacio
            txtPeso.text = vacio
            txtAltura.requestFocus()
            txtPeso.requestFocus()
        }

        //Codificación del boton regresar
        btnRegresar.setOnClickListener {
            finish()
        }
    }
}